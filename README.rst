 
SevenSegmentDisplay
===================

Description
-----------
Module to control a 7 segment display in Python 2 or Python 3

Controls any number of digits from 1 to however many the Raspberry Pi can 
support.

This module takes some of the work out of driving a 7 segment display.
It runs itself in a separate thread, allowing you to just set it and not
have to worry about continuously cycling the digits to see them.

**Dependencies**
::

    wiringpi

Usage:
------
.. code-block:: python

    #!/usr/bin/env python

    import SevenSegmentDisplay
    import time
    import random

    # wiringpi pin numbers
    segments = [0, 1, 2, 3, 4, 5, 6, 7]

    digits = [21, 22, 23, 24]

    # default value is True for common cathode
    # set to False for common_anode
    common_cathode = True

    seg_display = SevenSegmentDisplay.SevenSegmentDisplay(segments,
                                                          digits,
                                                          common_cathode=common_cathode)


    def custom():
        for _ in range(5):
            z = [[128 >> x for y in range(5)] for x in range(6)]

            for a in range(len(z)):
                print(z[a])
                seg_display.display(z[a])
                time.sleep(.1)

            for b in range(len(z) - 1, -1, -1):
                print(z[b])
                seg_display.display(z[b])
                time.sleep(.1)

        time.sleep(1)

        z = [[(128 >> x) for y in range(len(digits))] for x in [3, 6, 0]]
        for _ in range(5):
            for a in range(len(z) - 1, -len(z) - 1, -1):
                print(a, z[a])
                seg_display.display(z[a])
                time.sleep(.4)

        time.sleep(1)


    def random_stuff():
        for _ in range(5):
            time.sleep(1)
            z = [''.join(random.choice(["1", "0"]) for _ in range(8)) for a in range(len(digits))]
            z = list(map(int, z))
            print(z)
            seg_display.display(z)

        time.sleep(1)


    def user_input():
        for _ in range(5):
            seg_display.display(raw_input("Enter display string: "))
        time.sleep(1)


    def countdown():
        for y in range(10, 0, -1):
            print(y)
            seg_display.display(str(y))
            time.sleep(1)
        seg_display.clear()

    if __name__ == "__main__":

        try:
            print("Use individual segments")
            custom()
            random_stuff()

            print("Left align")
            seg_display.right_align(False)
            user_input()

            print("Right align with blanks")
            seg_display.right_align(True)
            seg_display.use_leading_zeros(False)
            countdown()

            print("use leading zeros")
            seg_display.use_leading_zeros(True)
            countdown()

        except (KeyboardInterrupt, EOFError) as error:
            print(error)
            pass

        finally:
            # call cancel to shut everything down
            seg_display.cancel()

            # wait to give the thread time to shutdown befor exiting the program
            seg_display.join()

METHODS:
--------
SevenSegmentDisplay.display(value)
::

    Set and format the given value to display
    Accepts strings and lists
        String is a string representing what you want to display.
        "1234", "78.9", "AbC", etc...

        Lists are list of binary literals representing the segments you want
        to turn on.
        [0b00000000, 0b11111111]
        Each index in the list represents a digit

        If you supply a list shorter then the number of digits you have
        it will be extended not padded with blanks

SevenSegmentDisplay.clear()
::

    clear and blank the display
    clear and display zero(s) if leading_zeros = True

SevenSegmentDisplay.cancel()
::

    Cancel the instance

SevenSegmentDisplay.use_leading_zeros(use)
::

    use leading zeros on the display
    default = False

SevenSegmentDisplay.right_align(use)
::

    if true text will be right aligned
    left aligned otherwise
    default = True

Pin outs
--------
::

                 A+, B+ and Pi2 B, and Zero models
                            +=========+
            POWER  3.3VDC   | 1 . . 2 |  5.0VDC   POWER
         I2C SDA1  GPIO  8  | 3 . . 4 |  5.0VDC   POWER
         I2C SCL1  GPIO  9  | 5 . . 6 |  GROUND
         GPCLK0    GPIO  7  | 7 . . 8 |  GPIO 15  TxD UART
                   GROUND   | 9 . . 10|  GPIO 16  RxD UART
                   GPIO  0  |11 . . 12|  GPIO  1  PCM_CLK/PWM0
                   GPIO  2  |13 . . 14|  GROUND
                   GPIO  3  |15 . . 16|  GPIO  4
            POWER  3.3VDC   |17 . . 18|  GPIO  5
         SPI MOSI  GPIO 12  |19 .   20|  GROUND
         SPI MISO  GPIO 13  |21 . . 22|  GPIO  6
         SPI SCLK  GPIO 14  |23 . . 24|  GPIO 10  CE0 SPI
                   GROUND   |25 . . 26|  GPIO 11  CE1 SPI
    I2C ID EEPROM  SDA0     |27 . . 28|  SCL0     I2C ID EEPROM
           GPCLK1  GPIO 21  |29 . . 30|  GROUND
           GPCLK2  GPIO 22  |31 . . 32|  GPIO 26  PWM0
             PWM1  GPIO 23  |33 . . 34|  GROUND
      PCM_FS/PWM1  GPIO 24  |35 . . 36|  GPIO 27
                   GPIO 25  |37 . . 38|  GPIO 28  PCM_DIN
                   GROUND   |39 . . 40|  GPIO 29  PCM_DOUT
                            +=========+

                           A and B models
                            +=========+
            POWER  3.3VDC   | 1 . . 2 |  5.0VDC   POWER
         I2C SDA0  GPIO  8  | 3 . . 4 |  DNC  
         I2C SCL0  GPIO  9  | 5 . . 6 |  GROUND
                   GPIO  7  | 7 . . 8 |  GPIO 15  TxD UART
                   DNC      | 9 . . 10|  GPIO 16  RxD UART
                   GPIO  0  |11 . . 12|  GPIO  1  PCM_CLK/PWM0
                   GPIO  2  |13 . . 14|  DNC
                   GPIO  3  |15 . . 16|  GPIO  4
                   DNC      |17 . . 18|  GPIO  5
         SPI MOSI  GPIO 12  |19 .   20|  DNC
         SPI MISO  GPIO 13  |21 . . 22|  GPIO  6
         SPI SCLK  GPIO 14  |23 . . 24|  GPIO 10  CE0 SPI
                   DNC      |25 . . 26|  GPIO 11  CE1 SPI
                            +=========+

     4 digit 7 segment display with decimal point and/or colon

                12         9           8          6
         +===============================================+
         |      11       12 11 10  9  8  7               |
         |   +=====+    +=====+     +=====+    +=====+   |
         |   |     |    |     |     |     |    |     |   |
         | 10|     |7   |     |     |     |    |     |   |
         |   |  5  |    |     |  x  |     |    |     |   |
         |   +==+==+    +=====+  3  +=====+    +=====+   |
         |   |     |    |     |  x  |     |    |     |   |
         |  1|     |4   |     |     |     |    |     |   |
         |   |     |    |     |     |     |    |     |   |
         |   +=====+ x3 +=====+ x   +=====+ x  +=====+ x |
         |      2        1  2  3  4  5  6                |
         +===============================================+
                12         9           8          6

    Wiring order for segments 11 7 4 2 1 10 6 3
    If your module is not equipped with resistors you 
    will need to place resistors on these line
    11 top
    7  top right
    4  bottom right
    2  bottom
    1  bottom left
    10 top left
    5  middle
    3  decimal place or colon (optional)

    digits are on pins 12 9 8 6
    resistors are not required
    digit 1 pin 12
    digit 2 pin 9
    digit 3 pin 8
    digit 4 pin 6

    example for B+
                11  7  4  2  1  10  5  3
    segments = [0 , 1, 2, 3, 4, 5 , 6, 7]
              12  9   8   6
    digits = [21, 22, 23, 24]
