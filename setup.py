from setuptools import setup


def readme():
    with open('README.rst') as f:
        return f.read()


setup(name='SevenSegmentDisplay',
      version='.1',
      description='Control a 7 segment display',
      long_description=readme(),
      url='https://bitbucket.org/tom_slick/SevenSegmentDisplay',
      author='Tom Enos',
      author_email='',
      license='',
      packages=['SevenSegmentDisplay'],
      install_requires=[
          'wiringpi',
      ],
      include_package_data=True,
      zip_safe=False)