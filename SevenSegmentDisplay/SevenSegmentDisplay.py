# coding=utf-8

import wiringpi
from time import sleep
from threading import Thread
import atexit


class SevenSegmentDisplay(Thread):
    """Use wiringpi to drive seven segment displays
       
       Running as a separate thread allows you to update
       the display and not have to worry about refreshing
       the display.
       Set your segment pins, there will only be 8.
       Set your digit pins, the total number of digits you have.
       Is it common cathode or common anode?
       Any expansion chips (mcp23017's, sr595's, etc.) see enable_device() for
       more details.
    """

    def __init__(self, segments, digits, common_cathode=True, devices=None):
        """Constructor

        :param segments: List of GPIO pins to use for display segments
        :type segments: list

        :param digits: List of GPIO pins to use for digits
        :type digits: list

        :param common_cathode: Using a common cathode display
        :type common_cathode: boolean

        :param devices: External devices to use.
        :type devices: dictionary
        """
        super(SevenSegmentDisplay, self).__init__()

        if not devices:
            devices = {}

        wiringpi.wiringPiSetup()
        atexit.register(self.exit_now)

        self.daemon = True
        self.cancelled = False
        self.leading_zeros = False

        self.displayed = ""
        self.on = 1
        self.off = 0
        self.pe = None

        self.bts = lambda bit: '{:08b}'.format(bit)

        self.devices = devices
        self.segments = segments
        self.digits = digits
        self.num_digits = len(self.digits)

        self.right_align(True)
        self.enable_device(self.devices)
        self.setup_pins()

        # no m, v, w, or x : Er represents an error (character not in dict)
        # % is the degree symbol ( ° ) or close to it
        self.values = {" ": 0b00000000, "0": 0b11111100, "1": 0b01100000,
                       "2": 0b11011010, "3": 0b11110010, "4": 0b01100110,
                       "5": 0b10110110, "6": 0b10111110, "7": 0b11100000,
                       "8": 0b11111110, "9": 0b11100110, "A": 0b11101110,
                       "b": 0b00111110, "C": 0b10011100, "c": 0b00011010,
                       "d": 0b01111010, "E": 0b10011110, "F": 0b10001110,
                       "G": 0b10111110, "H": 0b01101110, "h": 0b00101110,
                       "I": 0b01100000, "J": 0b01111000, "L": 0b00011100,
                       "k": 0b10101110, "n": 0b00101010, "O": 0b11111100,
                       "o": 0b00111010, "P": 0b11001110, "q": 0b11100110,
                       "r": 0b00001010, "S": 0b10110110, "T": 0b11100000,
                       "t": 0b01110010, "U": 0b01111100, "u": 0b00111000,
                       "Y": 0b01110110, "Z": 0b11011010, "-": 0b00000010,
                       "_": 0b00010000, "=": 0b00010010, "Er": 0b01010101,
                       "%": 0b11000110, '"': 0b01000100, "'": 0b01000000}

        if not common_cathode:
            self.on = 0
            self.off = 1

            for key in self.values:
                values[key] = (~values[key] & 0xFF)

        self.display(self.displayed)
        self.start()

    def exit_now(self):
        """Clean up on exit"""
        self.setup_pins(0)

    def cancel(self):
        """End this thread"""
        self.cancelled = True

    def run(self):
        """Overloaded Thread.run, update the display"""
        try:
            while not self.cancelled:
                for digit in range(len(self.digits)):
                    for seg in range(8):
                        wiringpi.digitalWrite(self.segments[seg], int(self.displayed[digit][seg]))
                    wiringpi.digitalWrite(self.digits[digit], self.off)
                    sleep(.0001)
                    wiringpi.digitalWrite(self.digits[digit], self.on)

        except (AttributeError, TypeError):
            pass

    def clear(self):
        """clear and blank the display
           clear and display zero(s) if leading_zeros = True
        """
        self.display("")

    def display(self, value):
        """Set and format the given value to display

           Accepts strings and lists
           ::

               String is a string representing what you want to display.
               "1234", "78.9", "AbC", etc...

               Lists are list of binary literals representing the segments you want
               to turn on.
               [0b00000000, 0b11111111]
               Each index in the list represents a digit

               If you supply a list shorter then the number of digits you have
               it will be extended not padded with blanks

        :param value: String or List of binary literals
        :type value: str list

        """
        formatted_value = list()

        if isinstance(value, list):
            for a in value:
                formatted_value.append(self.bts(a))

            self.displayed = self.extend_value(formatted_value)

        elif isinstance(value, str):
            for a in range(len(value)):
                if value[a] == ".":
                    if a == 0:
                        formatted_value.append(self.on)
                        continue

                    if formatted_value[-1] == self.on:
                        formatted_value.append(self.on)
                        continue

                    formatted_value[-1] = (formatted_value[-1] & ~(1 << 0)) | (self.on << 0)
                    continue

                formatted_value.append(self.values.get(value[a], self.values.get("Er")))

            for i in range(len(formatted_value)):
                formatted_value[i] = self.bts(formatted_value[i])

            if ":" in value:
                formatted_value[0] = (formatted_value[0] & ~(1 << 0)) | (self.on << 0)

            self.displayed = self.pe(formatted_value)
        else:
            for a in range(self.num_digits):
                formatted_value.append(self.bts(self.values.get("Er")))

            self.displayed = formatted_value

    def pad_value(self, formatted_value):
        """Pad the displayed value with leading zeros or spaces

        :param formatted_value: list of strings
        :type formatted_value: list
        :return: formatted_value
        :rtype list: list
       """
        if len(formatted_value) < self.num_digits:
            pad = self.num_digits - len(formatted_value)
            temp = list()

            if self.leading_zeros:
                for a in range(pad):
                    temp.append(self.bts(self.values.get("0")))
            else:
                for a in range(pad):
                    temp.append(self.bts(self.values.get(" ")))

            temp.extend(formatted_value)
            formatted_value = temp

        return formatted_value

    def extend_value(self, formatted_value):
        """extend the displayed value with trailing spaces

        :param formatted_value: list of strings
        :type formatted_value: list
        :return: formatted_value
        :rtype list: list
        """
        if len(formatted_value) < self.num_digits:
            ext = self.num_digits - len(formatted_value)
            for a in range(ext):
                formatted_value.append(self.bts(self.values.get(" ")))

        return formatted_value

    def use_leading_zeros(self, use=False):
        """Add leading zeros to display

        :param use: TrUe to use the False to not
        :type use: boolean
        """
        if use:
            self.leading_zeros = True
        else:
            self.leading_zeros = False

    def right_align(self, use=True):
        """Align text to the right

        :param use: True to use the False to not
        :type use: boolean
        """
        if use:
            self.pe = lambda v: self.pad_value(v)
        else:
            self.pe = lambda v: self.extend_value(v)

    def initialize(self):
        """Setup devices and initialize gpio pins as outputs"""
        self.enable_device(self.devices)
        self.setup_pins()

    @staticmethod
    def enable_device(devices):
        """
        enable the specified device
        
        format of devices
        dictionary 
            key = device type
            value = list containing a dictionary
                the dictionary contains the options for the device
        ::

            example for single device
            devices = {"sr595": [{"pinBase": "200",
                       "numPins": "8",
                       "dataPin": "0",
                       "clockPin": "1",
                       "latchPin": "2"}]}

            example format for multiple devices
            devices = {"mcp23s17": [{"pinBase": "100", "spiPort": "0", "devId": "0"},
                                    {"pinBase": "200", "spiPort": "0", "devId": "7"}]}

            example format for multiple mixed devices
            devices = {"mcp23s17": [{"pinBase": "100", "spiPort": "0", "devId": "0"}],
                       "sr595": [{"pinBase": "200",
                                  "numPins": "8",
                                  "dataPin": "0",
                                  "clockPin": "1",
                                  "latchPin": "2"}]}

        :param devices: Dictionary of devices to use
        :type devices: dictionary
        """

        try:
            for key in devices.keys():
                device = key
                device_slaves = devices[key]

                if device.lower() == "mcp23017":
                    for slave in device_slaves:
                        params = slave

                        wiringpi.mcp23017Setup(int(params['pinBase']),
                                               int(params['i2cAddress'], 16))

                elif device.lower() == "mcp23s17":
                    for slave in device_slaves:
                        params = slave
                        wiringpi.mcp23s17Setup(int(params['pinBase']),
                                               int(params['spiPort'], 16),
                                               int(params['devId']))

                elif device.lower() == "mcp23008":
                    for slave in device_slaves:
                        params = slave
                        wiringpi.mcp23008Setup(int(params['pinBase']),
                                               int(params['i2cAddress'], 16))

                elif device.lower() == "mcp23s08":
                    for slave in device_slaves:
                        params = slave
                        wiringpi.mcp23s08Setup(int(params['pinBase']),
                                               int(params['spiPort'], 16),
                                               int(params['devId']))

                elif device.lower() == "sr595":
                    for slave in device_slaves:
                        params = slave
                        wiringpi.sr595Setup(int(params['pinBase']),
                                            int(params['numPins']),
                                            int(params['dataPin']),
                                            int(params['clockPin']),
                                            int(params['latchPin']))

                elif device.lower() == "mcp23016":
                    for slave in device_slaves:
                        params = slave
                        wiringpi.mcp23016Setup(int(params['pinBase']),
                                               int(params['i2cAddress'], 16))

                elif device.lower() == "pcf8574":
                    for slave in device_slaves:
                        params = slave
                        wiringpi.pcf8574Setup(int(params['pinBase']),
                                              int(params['i2cAddress'], 16))
        except Exception as error:
            print("Error setting up devices, please check your devices settings.")
            print(error)

    def setup_pins(self, direction=1):
        """Set the gpio pins as outputs or inputs

        :param direction: 1 for out 0 for in
        :type direction: int
        """
        for pin in self.segments:
            wiringpi.pinMode(pin, direction)

        for pin in self.digits:
            wiringpi.pinMode(pin, direction)
